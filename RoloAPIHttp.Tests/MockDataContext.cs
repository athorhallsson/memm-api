﻿using RoloAPIHttp.Models;
using RoloAPIHttp.DAL;
using System.Data.Entity;

namespace RoloAPIHttp.Tests
{
    class MockDataContext : IAppDataContext
    {
        /// <summary>
        /// Sets up the fake database.
        /// </summary>
        public MockDataContext()
        {
            // We're setting our DbSets to be InMemoryDbSets rather than using SQL Server.
            Users = new InMemoryDbSet<ApplicationUser>();
            Comments = new InMemoryDbSet<Comment>();
            FriendConnections = new InMemoryDbSet<FriendConnection>();
            Images = new InMemoryDbSet<Image>();
            Playgrounds = new InMemoryDbSet<Playground>();
            PlaygroundConnections = new InMemoryDbSet<PlaygroundConnection>();
            PlaygroundRatings = new InMemoryDbSet<PlaygroundRating>();
            Posts = new InMemoryDbSet<Post>();
            CheckInConnections = new InMemoryDbSet<CheckInConnection>();
        }

        public IDbSet<ApplicationUser> Users { get; set; }
        public IDbSet<Comment> Comments { get; set; }
        public IDbSet<FriendConnection> FriendConnections { get; set; }
        public IDbSet<Image> Images { get; set; }
        public IDbSet<Playground> Playgrounds { get; set; }
        public IDbSet<PlaygroundConnection> PlaygroundConnections { get; set; }
        public IDbSet<PlaygroundFeature> PlaygroundFeatures { get; set; }
        public IDbSet<PlaygroundRating> PlaygroundRatings { get; set; }
        public IDbSet<Post> Posts { get; set; }
        public IDbSet<CheckInConnection> CheckInConnections { get; set; }

        public int SaveChanges()
        {
            // Pretend that each entity gets a database id when we hit save.
            int changes = 0;

            return changes;
        }

        public void Dispose()
        {
            // Do nothing!
        }
    }
}