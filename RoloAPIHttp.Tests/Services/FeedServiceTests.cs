﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoloAPIHttp.Services;
using RoloAPIHttp.Tests;
using RoloAPIHttp.Models;

namespace RoloAPIHttp.Tests.Services
{
    [TestClass()]
    public class FeedServiceTests
    {
        private FeedService service;
        private MockDataContext db = new MockDataContext();

        [TestInitialize]
        public void Initialize()
        {
            ApplicationUser bjoggi = new ApplicationUser { Id = "1", Name = "Bjorgvin Litli", UserName = "bjoggib", Email = "bla@bla.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 1, Path = "http://andri.pedro.is/images/bjoggib.jpg" }, LatestRead = DateTime.Now };
            ApplicationUser vissihaus = new ApplicationUser { Id = "2", Name = "Vissi Hauksson", UserName = "vissihaus", Email = "vissi@haus.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 2, Path = "http://andri.pedro.is/images/vissihaus.jpg" }, LatestRead = DateTime.Now };
            ApplicationUser andri = new ApplicationUser { Id = "3", Name = "Andri Mar", UserName = "andri", Email = "blabla@bla.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 3, Path = "http://andri.pedro.is/images/andri.jpg" } };
            ApplicationUser doctorinn = new ApplicationUser { Id = "4", Name = "Asgeir Thor", UserName = "doctorinn", Email = "doctor@doctor.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 4, Path = "http://andri.pedro.is/images/doctorinn.jpg" }, LatestRead = DateTime.Now };
            db.Users.Add(bjoggi);
            db.Users.Add(vissihaus);
            db.Users.Add(andri);
            db.Users.Add(doctorinn);

            FriendConnection fc1 = new FriendConnection { Id = 1, UserId = bjoggi, FriendId = vissihaus, Accepted = true };
            FriendConnection fc2 = new FriendConnection { Id = 2, UserId = andri, FriendId = bjoggi, Accepted = true };
            FriendConnection fc3 = new FriendConnection { Id = 3, UserId = andri, FriendId = vissihaus, Accepted = false };
            db.FriendConnections.Add(fc1);
            db.FriendConnections.Add(fc2);
            db.FriendConnections.Add(fc3);

            var playgroundList = new List<Playground>
            {
                new Playground {
                    Id=1,
                    Name="Freyjugöturóló",
                    Features=new PlaygroundFeature {InfantSwings=true, Swings=true, Sandbox=false, InfantSlide=false, Slide=true, InfantJungleGym=true, JungleGym=true, SeeSaw=false, RockingHorse=true, BalancingEquipment=true, Goal=false, Basket=false, PlayHouse=true},
                    Image= new Image{Path="http://andri.pedro.is/images/rolo_freyjugoturolo.jpg"}, 
                    Latitude=64.142618,
                    Longitude=-21.930805,
                },
                new Playground {
                    Id=2,
                    Name="Barónsborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=true, RockingHorse=false, BalancingEquipment=false, Goal=false, Basket=true, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_baronsborg.jpg"}, 
                    Latitude=64.142231,
                    Longitude= -21.920037,
                },
                new Playground {
                    Id=3,
                    Name="Njálsborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=true, BalancingEquipment=false, Goal=false, Basket=true, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_njalsborg.jpg"},
                    Latitude=64.144541,
                    Longitude=-21.928298,
                },
                new Playground {
                    Id=4,
                    Name="Grænuborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=false, BalancingEquipment=false, Goal=false, Basket=false, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_graenuborg.jpg"}, 
                    Latitude=64.141172,
                    Longitude=-21.928093,
                },
                new Playground {
                    Id=5,
                    Name="Austurbæjarskóli",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=false, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=false, BalancingEquipment=false, Goal=true, Basket=true, PlayHouse=false},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_austurbaejarskoli.jpg"}, 
                    Latitude=64.142129,
                    Longitude=-21.922389,
                }
            };

            playgroundList.ForEach(s => db.Playgrounds.Add(s));

            CheckInConnection cc1 = new CheckInConnection { Id = 1, UserId = bjoggi, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), DateCreated = DateTime.Now };
            CheckInConnection cc2 = new CheckInConnection { Id = 2, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 5).Single(), DateCreated = DateTime.Now };
            db.CheckInConnections.Add(cc1);
            db.CheckInConnections.Add(cc2);

            PlaygroundConnection pc1 = new PlaygroundConnection { Id = 1, UserId = andri, PlaygroundId = db.Playgrounds.Where(x => x.Id == 2).Single() };
            PlaygroundConnection pc2 = new PlaygroundConnection { Id = 2, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 4).Single() };
            db.PlaygroundConnections.Add(pc1);
            db.PlaygroundConnections.Add(pc2);

            PlaygroundRating pr1 = new PlaygroundRating { Id = 1, UserId = andri, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), Rating = 5 };
            PlaygroundRating pr2 = new PlaygroundRating { Id = 2, UserId = bjoggi, PlaygroundId = db.Playgrounds.Where(x => x.Id == 2).Single(), Rating = 1 };
            PlaygroundRating pr3 = new PlaygroundRating { Id = 3, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), Rating = 3 };
            db.PlaygroundRatings.Add(pr1);
            db.PlaygroundRatings.Add(pr2);
            db.PlaygroundRatings.Add(pr3);

            Post post1 = new Post { Id = 1, Text = "Post nr 1", AuthorId = bjoggi, DateCreated = DateTime.Now, DateModified = DateTime.Now, PlaygroundId = null };
            Post post2 = new Post { Id = 2, Text = "Post nr 2", AuthorId = vissihaus, DateCreated = DateTime.Now, DateModified = DateTime.Now, PlaygroundId = null };
            Post post3 = new Post { Id = 3, Text = "Post nr 3 a leikvoll", AuthorId = andri, DateCreated = DateTime.Now, DateModified = DateTime.Now, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single() };
            db.Posts.Add(post1);
            db.Posts.Add(post2);
            db.Posts.Add(post3);

            Comment comment1 = new Comment { Id = 1, Text = "Comment nr 1", AuthorId = bjoggi, DateCreated = DateTime.Now, PostId = post1 };
            Comment comment2 = new Comment { Id = 2, Text = "Comment nr 2", AuthorId = vissihaus, DateCreated = DateTime.Now, PostId = post1 };
            Comment comment3 = new Comment { Id = 3, Text = "Comment nr 3", AuthorId = andri, DateCreated = DateTime.Now, PostId = post1 };
            db.Comments.Add(comment1);
            db.Comments.Add(comment2);
            db.Comments.Add(comment3);

            db.SaveChanges();

            service = new FeedService(db);
        }

        [TestMethod()]
        public void GetUserFeedByIdTest()
        {
            // Arrange:
            var friendsForUser = (from f in db.Users
                                  where f.Id == "2" || f.Id == "3"
                                  select f).ToList();
            string userId = "1";
            var playgroundsForUser = new List<Playground>();
            // Act:
            var result = service.GetUserFeedById(friendsForUser, userId, playgroundsForUser);
            // Assert:
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod()]
        public void GetPostsByPlaygrIdTest()
        {
            // Arrange:
            int freyjugotuId = 1;
            // Act:
            var result = service.GetPostsByPlaygrId(freyjugotuId);
            // Assert:
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod()]
        public void AddPostTest()
        {
            // Arrange:
            string userId = "2";
            // Act:
            service.AddPost("Nýr post", userId, 1);
            // Assert:
            var result = db.Posts.Where(x => x.PlaygroundId != null && x.PlaygroundId.Id == 1).ToList();
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod()]
        public void GetPostsByFriendIdTest()
        {
            // Arrange:
            string vissiId = "2";
            // Act:
            var result = service.GetPostsByFriendId(vissiId);
            // Assert:
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod()]
        public void GetCommentViewModelsByPostIdTest()
        {
            // Arrange:
            int postId = 1;
            // Act:
            var result = service.GetCommentViewModelsByPostId(postId);
            // Assert:
            Assert.AreEqual(3, result.Count);
        }

        [TestMethod()]
        public void GetCommentViewModelsByPostIdTest2()
        {
            // Arrange:
            int postId = 2;
            // Act:
            var result = service.GetCommentViewModelsByPostId(postId);
            // Assert:
            Assert.IsTrue(result.ToList().Count == 0);
        }

        [TestMethod()]
        public void AddCommentTest()
        {
            // Arrange:
            int postId = 1;
            String andriId = "3";
            // Act:
            service.AddComment("Comment test texti", postId, andriId);
            // Assert:
            var result = (from c in db.Comments
                          select c).ToList();
            Assert.AreEqual(4, result.Count);
            Assert.AreEqual(1, result.Where(x => x.Text == "Comment test texti").ToList().Count);
        }

        [TestMethod()]
        public void CheckForNewPostsTest()
        {
            //List<ApplicationUser> friendList, string userId, List<Playground> playgrounds
            // Arrange:
            var friendsForUser = (from f in db.Users
                                  where f.Id == "2" || f.Id == "3"
                                  select f).ToList();
            string userId = "1";
            var playgroundsForUser = new List<Playground>();
            // Act:
            var result = service.GetUserFeedById(friendsForUser, userId, playgroundsForUser);
            // Assert:
            Assert.AreEqual(2, result.Count);
        }
    }
}
