﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoloAPIHttp.Services;
using RoloAPIHttp.Models;
using RoloAPIHttp.Tests;

namespace RoloAPIHttp.Tests.Services
{
    [TestClass()]
    public class ConnectionServiceTests
    {
        private ConnectionService service;
        private MockDataContext db = new MockDataContext();

        [TestInitialize]
        public void Initialize()
        {
            ApplicationUser bjoggi = new ApplicationUser { Id = "1", Name = "Bjorgvin Litli", UserName = "bjoggib", Email = "bla@bla.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 1, Path = "http://andri.pedro.is/images/bjoggib.jpg" } };
            ApplicationUser vissihaus = new ApplicationUser { Id = "2", Name = "Vissi Hauksson", UserName = "vissihaus", Email = "vissi@haus.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 2, Path = "http://andri.pedro.is/images/vissihaus.jpg" } };
            ApplicationUser andri = new ApplicationUser { Id = "3", Name = "Andri Mar", UserName = "andri", Email = "blabla@bla.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 3, Path = "http://andri.pedro.is/images/andri.jpg" } };
            ApplicationUser doctorinn = new ApplicationUser { Id = "4", Name = "Asgeir Thor", UserName = "doctorinn", Email = "doctor@doctor.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 4, Path = "http://andri.pedro.is/images/doctorinn.jpg" } };

            db.Users.Add(bjoggi);
            db.Users.Add(vissihaus);
            db.Users.Add(andri);
            db.Users.Add(doctorinn);

            FriendConnection fc1 = new FriendConnection { Id = 1, UserId = bjoggi, FriendId = vissihaus, Accepted = true };
            FriendConnection fc2 = new FriendConnection { Id = 2, UserId = andri, FriendId = bjoggi, Accepted = true };
            FriendConnection fc3 = new FriendConnection { Id = 3, UserId = andri, FriendId = vissihaus, Accepted = false };
            db.FriendConnections.Add(fc1);
            db.FriendConnections.Add(fc2);
            db.FriendConnections.Add(fc3);

            var playgroundList = new List<Playground>
            {
                new Playground {
                    Id=1,
                    Name="Freyjugöturóló",
                    Features=new PlaygroundFeature {InfantSwings=true, Swings=true, Sandbox=false, InfantSlide=false, Slide=true, InfantJungleGym=true, JungleGym=true, SeeSaw=false, RockingHorse=true, BalancingEquipment=true, Goal=false, Basket=false, PlayHouse=true},
                    Image= new Image{Path="http://andri.pedro.is/images/rolo_freyjugoturolo.jpg"}, 
                    Latitude=64.142618,
                    Longitude=-21.930805,
                },
                new Playground {
                    Id=2,
                    Name="Barónsborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=true, RockingHorse=false, BalancingEquipment=false, Goal=false, Basket=true, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_baronsborg.jpg"}, 
                    Latitude=64.142231,
                    Longitude= -21.920037,
                },
                new Playground {
                    Id=3,
                    Name="Njálsborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=true, BalancingEquipment=false, Goal=false, Basket=true, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_njalsborg.jpg"},
                    Latitude=64.144541,
                    Longitude=-21.928298,
                },
                new Playground {
                    Id=4,
                    Name="Grænuborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=false, BalancingEquipment=false, Goal=false, Basket=false, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_graenuborg.jpg"}, 
                    Latitude=64.141172,
                    Longitude=-21.928093,
                },
                new Playground {
                    Id=5,
                    Name="Austurbæjarskóli",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=false, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=false, BalancingEquipment=false, Goal=true, Basket=true, PlayHouse=false},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_austurbaejarskoli.jpg"}, 
                    Latitude=64.142129,
                    Longitude=-21.922389,
                }
            };

            playgroundList.ForEach(s => db.Playgrounds.Add(s));

            CheckInConnection cc1 = new CheckInConnection { Id = 1, UserId = bjoggi, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), DateCreated = DateTime.Now };
            CheckInConnection cc2 = new CheckInConnection { Id = 2, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 5).Single(), DateCreated = DateTime.Now };
            db.CheckInConnections.Add(cc1);
            db.CheckInConnections.Add(cc2);

            PlaygroundConnection pc1 = new PlaygroundConnection { Id = 1, UserId = andri, PlaygroundId = db.Playgrounds.Where(x => x.Id == 2).Single() };
            PlaygroundConnection pc2 = new PlaygroundConnection { Id = 2, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 4).Single() };
            db.PlaygroundConnections.Add(pc1);
            db.PlaygroundConnections.Add(pc2);

            PlaygroundRating pr1 = new PlaygroundRating { Id = 1, UserId = andri, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), Rating = 5 };
            PlaygroundRating pr2 = new PlaygroundRating { Id = 2, UserId = bjoggi, PlaygroundId = db.Playgrounds.Where(x => x.Id == 2).Single(), Rating = 1 };
            PlaygroundRating pr3 = new PlaygroundRating { Id = 3, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), Rating = 3 };
            db.PlaygroundRatings.Add(pr1);
            db.PlaygroundRatings.Add(pr2);
            db.PlaygroundRatings.Add(pr3);

            db.SaveChanges();

            service = new ConnectionService(db);
        }

        [TestMethod()]
        public void AddPlaygroundConnectionTest()
        {
            // Arrange:
            int freyjugoturoloId = 1;
            string bjoggiId = "1";
            // Act:
            service.AddPlaygroundConnection(bjoggiId, freyjugoturoloId);
            // Assert:
            var result = db.PlaygroundConnections.Where(x => x.UserId.Id == bjoggiId).ToArray();
            Assert.AreEqual(1, result.Count());
            Assert.AreEqual(freyjugoturoloId, result[0].PlaygroundId.Id);
        }

        [TestMethod()]
        public void AddCheckInConnectionTest()
        {
            // Arrange:
            int freyjugoturoloId = 1;
            string bjoggiId = "1";
            // Act:
            service.AddCheckInConnection(bjoggiId, freyjugoturoloId);
            // Assert:
            var result = db.CheckInConnections.Where(x => x.UserId.Id == bjoggiId).ToArray();
            Assert.IsTrue(result.Count() == 0);
        }

        [TestMethod()]
        public void AddCheckInConnectionTest2()
        {
            // Arrange:
            int baronsborgId = 2;
            string bjoggiId = "1";
            // Act:
            service.AddCheckInConnection(bjoggiId, baronsborgId);

            var result = db.CheckInConnections.Where(x => x.UserId.Id == bjoggiId).ToList();
            // Assert:

            Assert.AreEqual(1, result.Count);
        }

        [TestMethod()]
        public void AddFriendConnectionTest()
        {
            // Arrange:
            string doctorId = "4";
            string andriId = "3";
            // Act:
            service.AddFriendConnection(doctorId, andriId);
            var result = db.FriendConnections.Where(x => x.UserId.Id == doctorId && x.FriendId.Id == andriId).ToList();
            var result2 = result.Where(x => x.UserId.Id == doctorId).Single();
            // Assert:
            Assert.AreEqual(1, result.Count);
            Assert.IsFalse(result2.Accepted);
        }

        [TestMethod()]
        public void RemoveFriendConnectionTest()
        {
            // Arrange:
            string vissiId = "2";
            string andriId = "3";
            // Act:
            service.RemoveFriendConnection(vissiId, andriId);

            var result = db.FriendConnections.Where(x => x.UserId.Id == vissiId && x.FriendId.Id == andriId).SingleOrDefault();
            // Assert:
            Assert.IsNull(result);
        }

        [TestMethod()]
        public void AcceptFriendConnectionTest()
        {
            // Arrange:
            string vissiId = "2";
            string andriId = "3";
            // Act:
            service.AcceptFriendConnection(vissiId, andriId);

            var result = db.FriendConnections.Where(x => x.UserId.Id == andriId && x.FriendId.Id == vissiId).SingleOrDefault();
            // Assert:
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Accepted);
        }

        [TestMethod()]
        public void AcceptFriendConnectionTest2()
        {
            // Arrange:
            string doctorId = "4";
            string andriId = "3";
            // Act:
            service.AcceptFriendConnection(andriId, doctorId);

            var result = db.FriendConnections.Where(x => x.UserId.Id == andriId && x.FriendId.Id == doctorId).SingleOrDefault();
            // Assert:
            Assert.IsNull(result);
        }

        [TestMethod()]
        public void CheckPendingRequestTest()
        {
            // Arrange:
            string vissiId = "2";
            string andriUserName = "andri";
            // Act:
            bool result = service.CheckPendingRequest(vissiId, andriUserName);
            // Assert:
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void CheckPendingRequestTest2()
        {
            // Arrange:
            string doctorId = "4";
            string andriId = "3";
            // Act:
            bool result = service.CheckPendingRequest(doctorId, andriId);
            // Assert:
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void CheckFriendRequestSentTest()
        {
            // Arrange:
            string vissiUserName = "vissihaus";
            string andriId = "3";
            // Act:
            bool result = service.CheckFriendRequestSent(andriId, vissiUserName);
            // Assert:
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void CheckFriendRequestSentTest2()
        {
            // Arrange:
            string vissiUserName = "vissihaus";
            string bjoggiId = "1";
            // Act:
            bool result = service.CheckFriendRequestSent(bjoggiId, vissiUserName);
            // Assert:
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void CheckIfFriendsTest()
        {
            // Arrange:
            string vissiUserName = "vissihaus";
            string andriId = "3";
            // Act:
            bool result = service.CheckIfFriends(andriId, vissiUserName);
            // Assert:
            Assert.IsFalse(result);
        }

        [TestMethod()]
        public void CheckIfFriendsTest2()
        {
            // Arrange:
            string vissiUserName = "vissihaus";
            string bjoggiId = "1";
            // Act:
            bool result = service.CheckIfFriends(bjoggiId, vissiUserName);
            // Assert:
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void CheckIfFriendsTest3()
        {
            // Arrange:
            string bjoggiUserName = "bjoggib";
            string vissiId = "2";
            // Act:
            bool result = service.CheckIfFriends(vissiId, bjoggiUserName);
            // Assert:
            Assert.IsTrue(result);
        }

        [TestMethod()]
        public void GetFriendNotificationsTest()
        {
            // Arrange:
            string vissiId = "2";
            // Act:
            var result = service.GetFriendNotifications(vissiId);
            // Assert:
            Assert.AreEqual(1, result);
        }

        [TestMethod()]
        public void GetFriendNotificationsTest2()
        {
            // Arrange:
            string andriId = "1";
            // Act:
            var result = service.GetFriendNotifications(andriId);
            // Assert:
            Assert.AreEqual(0, result);
        }
    }
}