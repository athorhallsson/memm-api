﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RoloAPIHttp.Tests;
using RoloAPIHttp.Services;
using RoloAPIHttp.Models;

namespace RoloAPIHttp.Tests.Services
{
    [TestClass()]
    public class UserServiceTest
    {
        private UserService service;
        private MockDataContext db = new MockDataContext();

        [TestInitialize]
        public void Initialize()
        {   
            ApplicationUser bjoggi = new ApplicationUser { Id = "1", Name = "Bjorgvin Litli", UserName = "bjoggib", Email = "bla@bla.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 1, Path = "http://andri.pedro.is/images/bjoggib.jpg" }, LatestRead = DateTime.Now };
            ApplicationUser vissihaus = new ApplicationUser { Id = "2", Name = "Vissi Hauksson", UserName = "vissihaus", Email = "vissi@haus.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 2, Path = "http://andri.pedro.is/images/vissihaus.jpg" }, LatestRead = DateTime.Now };
            ApplicationUser andri = new ApplicationUser { Id = "3", Name = "Andri Mar", UserName = "andri", Email = "blabla@bla.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 3, Path = "http://andri.pedro.is/images/andri.jpg" }, LatestRead = DateTime.Now };
            ApplicationUser doctorinn = new ApplicationUser { Id = "4", Name = "Asgeir Thor", UserName = "doctorinn", Email = "asgeir@doctor.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 4, Path = "http://andri.pedro.is/images/doctorinn.jpg" }, LatestRead = DateTime.Now };
            ApplicationUser kalli = new ApplicationUser { Id = "5", Name = "Karl Karlsson", UserName = "kalli", Email = "kalli@geimfari.is", DateOfBirth = DateTime.Now, ProfilePic = new Image { Id = 5, Path = "http://andri.pedro.is/images/kalli.jpg" }, LatestRead = DateTime.Now };

            db.Users.Add(bjoggi);
            db.Users.Add(vissihaus);
            db.Users.Add(andri);
            db.Users.Add(doctorinn);
            db.Users.Add(kalli);

            FriendConnection fc1 = new FriendConnection { Id = 1, UserId = bjoggi, FriendId = vissihaus, Accepted = true };
            FriendConnection fc2 = new FriendConnection { Id = 2, UserId = andri, FriendId = bjoggi, Accepted = true };
            FriendConnection fc3 = new FriendConnection { Id = 3, UserId = andri, FriendId = vissihaus, Accepted = false };
            db.FriendConnections.Add(fc1);
            db.FriendConnections.Add(fc2);
            db.FriendConnections.Add(fc3);

            var playgroundList = new List<Playground>
            {
                new Playground {
                    Id=1,
                    Name="Freyjugöturóló",
                    Features=new PlaygroundFeature {InfantSwings=true, Swings=true, Sandbox=false, InfantSlide=false, Slide=true, InfantJungleGym=true, JungleGym=true, SeeSaw=false, RockingHorse=true, BalancingEquipment=true, Goal=false, Basket=false, PlayHouse=true},
                    Image= new Image{Path="http://andri.pedro.is/images/rolo_freyjugoturolo.jpg"}, 
                    Latitude=64.142618,
                    Longitude=-21.930805,
                },
                new Playground {
                    Id=2,
                    Name="Barónsborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=true, RockingHorse=false, BalancingEquipment=false, Goal=false, Basket=true, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_baronsborg.jpg"}, 
                    Latitude=64.142231,
                    Longitude= -21.920037,
                },
                new Playground {
                    Id=3,
                    Name="Njálsborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=true, BalancingEquipment=false, Goal=false, Basket=true, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_njalsborg.jpg"},
                    Latitude=64.144541,
                    Longitude=-21.928298,
                },
                new Playground {
                    Id=4,
                    Name="Grænuborg",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=true, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=false, BalancingEquipment=false, Goal=false, Basket=false, PlayHouse=true},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_graenuborg.jpg"}, 
                    Latitude=64.141172,
                    Longitude=-21.928093,
                },
                new Playground {
                    Id=5,
                    Name="Austurbæjarskóli",
                    Features=new PlaygroundFeature {InfantSwings=false, Swings=true, Sandbox=false, InfantSlide=false, Slide=true, InfantJungleGym=false, JungleGym=true, SeeSaw=false, RockingHorse=false, BalancingEquipment=false, Goal=true, Basket=true, PlayHouse=false},
                    Image= new Image {Path="http://andri.pedro.is/images/rolo_austurbaejarskoli.jpg"}, 
                    Latitude=64.142129,
                    Longitude=-21.922389,
                }
            };

            playgroundList.ForEach(s => db.Playgrounds.Add(s));

            CheckInConnection cc1 = new CheckInConnection { Id = 1, UserId = bjoggi, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), DateCreated = DateTime.Now };
            CheckInConnection cc2 = new CheckInConnection { Id = 2, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 5).Single(), DateCreated = DateTime.Now };
            db.CheckInConnections.Add(cc1);
            db.CheckInConnections.Add(cc2);

            PlaygroundConnection pc1 = new PlaygroundConnection { Id = 1, UserId = andri, PlaygroundId = db.Playgrounds.Where(x => x.Id == 2).Single() };
            PlaygroundConnection pc2 = new PlaygroundConnection { Id = 2, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 4).Single() };
            db.PlaygroundConnections.Add(pc1);
            db.PlaygroundConnections.Add(pc2);

            PlaygroundRating pr1 = new PlaygroundRating { Id = 1, UserId = andri, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), Rating = 5 };
            PlaygroundRating pr2 = new PlaygroundRating { Id = 2, UserId = bjoggi, PlaygroundId = db.Playgrounds.Where(x => x.Id == 2).Single(), Rating = 1 };
            PlaygroundRating pr3 = new PlaygroundRating { Id = 3, UserId = vissihaus, PlaygroundId = db.Playgrounds.Where(x => x.Id == 1).Single(), Rating = 3 };
            db.PlaygroundRatings.Add(pr1);
            db.PlaygroundRatings.Add(pr2);
            db.PlaygroundRatings.Add(pr3);

            db.SaveChanges();

            service = new UserService(db);
        }

        [TestMethod()]
        public void FriendsForBjoggiTest()
        {
            // Arrange:
            string bjoggiId = "1";
            // Act:
            var friends = service.GetAllFriendsForUser(bjoggiId);
            // Assert:
            Assert.AreEqual(2, friends.Count);
            Assert.AreNotEqual(friends, service.GetAllFriendsForUser("2"));
            Assert.AreNotEqual(friends, service.GetAllFriendsForUser("0"));
            foreach (var item in friends)
            {
                Assert.AreNotEqual(item.Id, "1");
            }
        }

        [TestMethod()]
        public void GetInfoForMeBjoggiTest()
        {
            // Arrange:
            string bjoggiId = "1";
            // Act:
            var user = service.GetInfoForMe(bjoggiId);
            var result = (from u in db.Users
                         where u.Id == bjoggiId
                         select u).SingleOrDefault();
            // Assert:
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Name, user.Name);  
        }

        [TestMethod()]
        public void GetProfilePicForBjoggiTest()
        {
            // Arrange:
            string bjoggiId = "1";
            // Act:
            var user = service.GetInfoForMe(bjoggiId);
            var result = (from u in db.Users
                          where u.Id == bjoggiId
                          select u).SingleOrDefault();
            // Assert:
            Assert.AreEqual(result.ProfilePic ,user.ProfilePic);
        }

        [TestMethod()]
        public void GetInfoForUserVissiTest()
        {
            // Arrange:
            string vissiUserName = "vissihaus";
            // Act:
            var user = service.GetInfoForUser(vissiUserName);
            var result = (from u in db.Users
                          where u.UserName == vissiUserName
                          select u).Single();
            // Assert:
            Assert.AreEqual(user.User.Name, result.Name);
            Assert.AreEqual(user.User.DateOfBirth, result.DateOfBirth);
            Assert.AreEqual(user.User.Id, result.Id);
        }

        [TestMethod()]
        public void GetRequestsViewModelsForUserVissi()
        {
            // Arrange:
            string vissiId = "2";
            // Act:
            var userlist = service.GetRequestsViewModelsForUser(vissiId);
            //Assert
            Assert.IsNotNull(userlist);
            Assert.AreEqual(userlist.Count, 1);  
        }

        [TestMethod()]
        public void GetAllFriendsForUserTest()
        {
            // Arrange:
            string andriId = "3";
            // Act:
            var userlist = service.GetAllFriendsForUser(andriId);
            //Assert
            Assert.IsNotNull(userlist);
            Assert.AreEqual(userlist.Count, 1);  
        }

        [TestMethod()]
        public void GetAllRequestsForUserVissiTest()
        {
            // Arrange:
            string vissiId = "2";
            // Act:
            var userlist = service.GetAllRequestsForUser(vissiId);
            //Assert
            Assert.IsNotNull(userlist);
            Assert.AreEqual(userlist.Count, 1);  
        }

        [TestMethod()]
        public void GetAllRequestsForUserAndriTest()
        {
            // Arrange:
            string andriId = "3";
            // Act:
            var userlist = service.GetAllRequestsForUser(andriId);
            //Assert
            Assert.IsNotNull(userlist);
            Assert.AreEqual(0, userlist.Count);
        }

        [TestMethod()]
        public void makeUserListViewModelAndriTest()
        {
            // Arrange:
            var andri = (from u in db.Users
                         where u.Id == "3"
                         select u).Single();
            // Act:
            var result = service.makeUserListViewModel(andri);
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Andri Mar", result.Name);
            Assert.AreEqual("", result.ActivePlaygroundName);
        }

        [TestMethod()]
        public void makeUserListViewModelBjoggiTest()
        {
            // Arrange:
            var bjoggi = (from u in db.Users
                         where u.Id == "1"
                         select u).Single();
            // Act:
            var result = service.makeUserListViewModel(bjoggi);
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Bjorgvin Litli", result.Name);
            Assert.AreEqual("Freyjugöturóló", result.ActivePlaygroundName);
        }

        [TestMethod()]
        public void GetRequestUserViewModelsForSearchTest()
        {
            // Arrange:
            string vissiId = "2";
            string searchString = "And";
            // Act:
            var result = service.GetRequestUserViewModelsForSearch(searchString, vissiId);
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod()]
        public void GetRequestUsersFromSearchTest()
        {
            // Arrange:
            string vissiId = "2";
            string searchString = "dri";
            // Act:
            var result = service.GetRequestUsersFromSearch(searchString, vissiId);
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod()]
        public void GetFriendsUserViewModelsForSearchTest()
        {
            // Arrange:
            string bjoggiId = "1";
            string searchString = "i";
            // Act:
            var result = service.GetFriendsUserViewModelsForSearch(searchString, bjoggiId);
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod()]
        public void GetFriendsUserViewModelsForSearchTest2()
        {
            // Arrange:
            string bjoggiId = "1";
            string searchString = "Vissi";
            // Act:
            var result = service.GetFriendsUserViewModelsForSearch(searchString, bjoggiId);
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod()]
        public void GetFriendsUsersFromSearchTest()
        {
            // Arrange:
            string andriId = "3";
            string searchString = "Vissi";
            // Act:
            var result = service.GetFriendsUsersFromSearch(searchString, andriId);
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod()]
        public void GetOthersUserViewModelsForSearchTest()
        {
            // Arrange:
            string andriId = "3";
            string searchString = "arl";
            // Act:
            var result = service.GetOthersUserViewModelsForSearch(searchString, andriId);
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod()]
        public void GetOthersUsersFromSearchTest()
        {
            // Arrange:
            string andriId = "3";
            string searchString = "Asgeir";
            // Act:
            var result = service.GetOthersUserViewModelsForSearch(searchString, andriId);
            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
        }

        [TestMethod()]
        public void UpdateLatestReadTest()
        {
            // Arrange:
            var vissi = (from u in db.Users
                         where u.Id == "2"
                         select u).Single();
            DateTime previousLatestRead = vissi.LatestRead; 
            // Act:
            service.UpdateLatestRead(vissi.Id);
            //Assert
            Assert.IsTrue(previousLatestRead < vissi.LatestRead);
        }

        [TestMethod()]
        public void AddImageToUserTest()
        {
            // Arrange:
            ApplicationUser andri = (from u in db.Users
                                     where u.Id == "3"
                                     select u).Single();
            Image previousImage = andri.ProfilePic;
            Image newImage = new Image { Id = 6, Path = "testImage" };
            // Act:
            service.AddImageToUser(newImage, andri.Id);
            //Assert
            Assert.IsFalse(previousImage == andri.ProfilePic);
            Assert.AreEqual(newImage.Path, andri.ProfilePic.Path); 
        }
    }
}
