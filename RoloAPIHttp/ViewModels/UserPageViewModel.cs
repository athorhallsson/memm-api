﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoloAPIHttp.ViewModels
{
    public class UserPageViewModel
    {
        public MePageViewModel User { get; set; }
        public List<UserPlaygroundListModel> PlaygroundList { get; set; }
        public bool didSendFriendRequest { get; set; }
        public bool hasPendingRequestFromUser { get; set; }
        public bool areFriends { get; set; }
    }
}