﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RoloAPIHttp.Models;

namespace RoloAPIHttp.ViewModels
{
    public class UserListViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Image ProfilePic { get; set; }
        public string ActivePlaygroundName { get; set; }
        public string Username { get; set; }
    }
}