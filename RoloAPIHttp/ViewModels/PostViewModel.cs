﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RoloAPIHttp.Models;

namespace RoloAPIHttp.ViewModels
{
    public class PostViewModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public UserListViewModel Author { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public Image PostImage { get; set; }
        public List<CommentViewModel> Comments { get; set; }
        public int PlaygroundId { get; set; }
        public string PlaygroundName { get; set; }
    }
}