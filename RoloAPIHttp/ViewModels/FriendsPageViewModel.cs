﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoloAPIHttp.ViewModels
{
    public class FriendsPageViewModel
    {
        public List<UserListViewModel> Requests { get; set; }
        public List<UserListViewModel> Friends { get; set; }
    }
}