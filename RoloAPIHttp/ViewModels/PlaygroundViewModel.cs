﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RoloAPIHttp.Models;

namespace RoloAPIHttp.ViewModels
{
    public class PlaygroundViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public PlaygroundFeature Features { get; set; }
        public Image PlaygroundImage { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public List<PostViewModel> Posts { get; set; }
        public List<UserListViewModel> ActiveFriends { get; set; }
        public bool CurrentUserIsCheckedIn { get; set; }
        public bool CurrentUserIsFollowing { get; set; }
        public int Range { get; set; }
        public double Rating { get; set; }
        public PlaygroundRating CurrentUserRating { get; set; }
    }
}