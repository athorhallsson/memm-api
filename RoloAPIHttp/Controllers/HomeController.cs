﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RoloAPIHttp.DAL;
using RoloAPIHttp.Models;
using RoloAPIHttp.Services;
using RoloAPIHttp.Filters;

namespace RoloAPIHttp.Controllers
{
    public class HomeController : Controller
    {
        [ExceptionFilter]
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
          
            return View();
        }
    }
}
