﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RoloAPIHttp.Models;
using RoloAPIHttp.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using RoloAPIHttp.ViewModels;
using System.IO;
using System.Web;
using RoloAPIHttp.DAL;
using System.Text;
using System.Drawing.Imaging;
using System.Drawing;
using RoloAPIHttp.Filters;

namespace RoloAPIHttp.Controllers
{
    [ExceptionFilter]
    [Authorize]
    public class RpcController : ApiController
    {
        private PlaygroundService pService = new PlaygroundService(null);
        private UserService uService = new UserService(null);
        private FeedService fService = new FeedService(null);
        private ConnectionService cService = new ConnectionService(null);

       // Me

        public MePageViewModel GetUserInfo()
        {
            string userId = User.Identity.GetUserId();
            MePageViewModel model = uService.GetInfoForMe(userId);
            return model;
        }
        /*
               [HttpPost]
               public  HttpResponseMessage UploadProfilePicture()
               {
                  string apiKey = "aTl2_Eg_Axd7n7D6Hm0dmMRK9hwfZwk5";
                   string tinyPNG = "https://api.tinify.com/shrink";
                   WebClient client = new WebClient();
                   string authorizationString = Convert.ToBase64String(Encoding.UTF8.GetBytes("api:" + apiKey));
                   client.Headers.Add(HttpRequestHeader.Authorization, "Basic" + authorizationString); 
           
                   HttpResponseMessage result = null;
                   var httpRequest = HttpContext.Current.Request;
                   if (httpRequest.Files.Count > 0)
                   {
                       String userId = User.Identity.GetUserId();
                       var docfiles = new List<string>();
                       foreach (string file in httpRequest.Files)
                       {
                           var postedFile = httpRequest.Files[file];
                           var filename = userId + "_" + postedFile.FileName;
                           try
                           {
                               client.UploadData(tinyPNG, File.ReadAllBytes(postedFile.FileName));
                               client.DownloadFile(client.ResponseHeaders["Location"], filename);
                           }
                           catch (WebException ex)
                           {
                               Console.WriteLine("Output failed: " + ex.Message);
                           }
                           var filePath = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\" + filename; 
                           postedFile.SaveAs(filePath);

                           Image profilePic = new Image { Path = String.Format("/Data/{0}", filename) };
                           uService.AddImageToUser(profilePic, userId);
 
                           docfiles.Add(filePath);
                       }
                       result = Request.CreateResponse(HttpStatusCode.Created, docfiles);
                   }
                   else
                   {
                       result = Request.CreateResponse(HttpStatusCode.BadRequest);
                   }
                   return result;
               }
       */

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        [HttpPost]
        public HttpResponseMessage UploadProfilePicture()
        {

            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                String userId = User.Identity.GetUserId();
                var docfiles = new List<string>();
                foreach (string file in httpRequest.Files)
                {
                    var postedFile = httpRequest.Files[file];
                 //   DateTime time = DateTime.Now;
                 //   var filename = userId + "_" + time.ToShortDateString() + "_" + postedFile.FileName;
                    var filename = userId + "_" + postedFile.FileName;
                    var filePath = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\" + filename;

                    // Get a bitmap.
                    System.Drawing.Bitmap uploadedImage = new System.Drawing.Bitmap(postedFile.InputStream); 
                    Bitmap resizedImage = new Bitmap(500, 500);
                    using (Graphics g = Graphics.FromImage(resizedImage))
                    {
                        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                        g.DrawImage(uploadedImage, 0, 0, 500, 500);
                    }

                    ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);

                    // Create an Encoder object based on the GUID 
                    // for the Quality parameter category.
                    System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;

                    // Create an EncoderParameters object. 
                    // An EncoderParameters object has an array of EncoderParameter 
                    // objects. In this case, there is only one 
                    // EncoderParameter object in the array.
                    EncoderParameters myEncoderParameters = new EncoderParameters(1);

                    EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 50L);
                    myEncoderParameters.Param[0] = myEncoderParameter;
                    resizedImage.Save(filePath, jpgEncoder, myEncoderParameters);

                    RoloAPIHttp.Models.Image profilePic = new RoloAPIHttp.Models.Image { Path = String.Format("/Data/{0}", filename) };
                    uService.AddImageToUser(profilePic, userId);

                    docfiles.Add(filePath);
                }
                result = Request.CreateResponse(HttpStatusCode.Created, docfiles);
            }
            else
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            return result;
        }


        // Playgrounds

        public List<UserPlaygroundListModel> MyPlaygrounds()
        {
            return pService.getPlaygroundListForUser(User.Identity.GetUserId());
        }

        // Gets nearby playgrounds for registered user
        public List<UserPlaygroundListModel> NearbyPlaygroundsUser(string lat, string lon)
        {
                Location userLocation = new Location();
                userLocation.Latitude = double.Parse(lat);
                userLocation.Longitude = double.Parse(lon);

                return pService.GetNearbyPlaygrounds(userLocation, User.Identity.GetUserId());
        }

        public List<UserPlaygroundListModel> NearbyPlaygroundsGuest(Location loc)
        {
            String userId = User.Identity.GetUserId();
            return pService.GetNearbyPlaygrounds(loc, userId);
        }

        public PlaygroundViewModel PlaygroundInfo(int id)
        {
            var listOfPosts = fService.GetPostsByPlaygrId(id);
            return pService.GetPlaygroundInfo(id, User.Identity.GetUserId(), listOfPosts);
        }

        public PlaygroundViewModel GuestPlaygroundInfo(int id)
        {
            return pService.GetGuestPlaygroundViewModel(id);
        }

        // Feed

        public List<PostViewModel> Feed()
        {
            string userId = User.Identity.GetUserId();
            List<ApplicationUser> friendList = uService.GetAllFriendsForUser(userId);
            List<Playground> playgrounds = pService.GetPlaygroundsForUser(userId);
            return fService.GetUserFeedById(friendList, userId, playgrounds);
        }

        [HttpPost]
        public string AddCommentToPostUser(AddCommentBindingModel model)
        {
            int postIdInt = Convert.ToInt32(model.postId);
            string userId = User.Identity.GetUserId();
            fService.AddComment(model.commentText, postIdInt, userId);

            return "OK";
        }

        [HttpPost]
        public string AddCommentToPostPlayground(string commentText, string postId, string playgroundId)
        {
            int postIdInt = Convert.ToInt32(postId);
            int playgroundIdInt = Convert.ToInt32(playgroundId);
            fService.AddComment(commentText, postIdInt, User.Identity.GetUserId());

            return "OK";
        }

        [HttpPost]
        public string AddPostToUser(AddPostBindingModel model)
        {
            string userId = User.Identity.GetUserId();
            fService.AddPost(model.postText, userId, null);

            return "OK";
        }

        [HttpPost]
        public string AddPostToPlayground(string postText, string playgroundId)
        {
            int playgroundIdInt = Convert.ToInt32(playgroundId);
            string userId = User.Identity.GetUserId();
            fService.AddPost(postText, userId, playgroundIdInt);

            return "OK";
        }

        [HttpPost]
        public PostViewModel GetPost(string postId)
        {
            // Moguleg oryggishola
            int postIdInt = Convert.ToInt32(postId);

            return fService.GetPost(postIdInt);
        }


        // Friends

        public FriendsPageViewModel Friend()
        {
            string userId = User.Identity.GetUserId();
            FriendsPageViewModel model = new FriendsPageViewModel();
            model.Friends = uService.GetFriendsViewmodelsForUser(userId);
            model.Requests = uService.GetRequestsViewModelsForUser(userId);
            return model;
        }

        public UserPageViewModel UserPage(string username)
        {
            UserPageViewModel model = uService.GetInfoForUser(username);
            string userId = User.Identity.GetUserId(); 
            model.hasPendingRequestFromUser = cService.CheckPendingRequest(userId, username);
            model.didSendFriendRequest = cService.CheckFriendRequestSent(userId, username);
            model.areFriends = cService.CheckIfFriends(userId, username);
            return model;
        }



        // Connection

        // Creates a PlaygroundConnection between a specific playground a the current user
        [HttpPost]
        public string FollowPlayground(string playgroundId)
        {
            int playgroundIdInt = int.Parse(playgroundId);
            string userId = User.Identity.GetUserId();
            cService.AddPlaygroundConnection(userId, playgroundIdInt);
            return "OK";
        }

        // Creates or deletes a CheckInConnection between a specific playground a the current user
        [HttpPost]
        public string CheckInPlayground(string playgroundId)
        {
            int playgroundIdInt = int.Parse(playgroundId); 
            cService.AddCheckInConnection(User.Identity.GetUserId(), playgroundIdInt);
            return "OK";
        }

        // Creates a FriendConnection between the current user and the selected user
        [HttpPost]
        public string AddFriend(string friendId)
        {
            cService.AddFriendConnection(User.Identity.GetUserId(), friendId);
            return "OK";
        }

        // Deletes a FriendConnection between the current user and the selected user
        [HttpPost]
        public string RemoveFriend(string friendId)
        {
            cService.RemoveFriendConnection(User.Identity.GetUserId(), friendId);
            return "OK";
        }

        // Approves a previosly made FriendConnection which another user made to the current user
        [HttpPost]
        public string AcceptFriend(string friendId)
        {
            cService.AcceptFriendConnection(User.Identity.GetUserId(), friendId);
            return "OK";
        }

        // Removes a previosly made FriendConnection which another user made to the current user
        [HttpPost]
        public string DeclineFriend(string friendId)
        {
            cService.RemoveFriendConnection(User.Identity.GetUserId(), friendId);
            return "OK";
        }



        // Search

        // Searches for Users which include a specified string
        [HttpPost]
        public List<List<UserListViewModel>> SearchUsers(string search)
        {
            string userId = User.Identity.GetUserId();

            List<UserListViewModel> requests = new List<UserListViewModel>();
            requests = uService.GetRequestUserViewModelsForSearch(search, userId);
            List<UserListViewModel> friends = new List<UserListViewModel>();
            friends = uService.GetFriendsUserViewModelsForSearch(search, userId);
            List<UserListViewModel> others = new List<UserListViewModel>();
            others = uService.GetOthersUserViewModelsForSearch(search, userId);

            List<List<UserListViewModel>> model = new List<List<UserListViewModel>>();
            model.Add(requests);
            model.Add(friends);
            model.Add(others);
            return model;
        }

        // Searches for playgrounds for unregistered users
        [HttpPost]
        public List<GuestPlaygroundListModel> GuestSearchPlaygrounds(string search, string lat, string lon)
        {
            Location guestLocation = new Location();
            guestLocation.Latitude = double.Parse(lat);
            guestLocation.Longitude = double.Parse(lon);

            return pService.GuestGetNearbyPlaygroundViewModelsForSearch(search, guestLocation);
        }

        // Searches for playgrounds for registered users
        [HttpPost]
        public List<List<UserPlaygroundListModel>> UserSearchPlaygrounds(string search, string lat, string lon)
        {
            string userId = User.Identity.GetUserId();

            Location Location = new Location();
            Location.Latitude = double.Parse(lat);
            Location.Longitude = double.Parse(lon);

            List<UserPlaygroundListModel> myPlaygrounds = new List<UserPlaygroundListModel>();
            List<UserPlaygroundListModel> nearbyPlaygrounds = new List<UserPlaygroundListModel>();
            myPlaygrounds = pService.UserGetMyPlaygroundViewModelsForSearch(search, Location, userId);
            nearbyPlaygrounds = pService.UserGetNearbyPlaygroundViewModelsForSearch(search, Location, User.Identity.GetUserId());

            List<List<UserPlaygroundListModel>> model = new List<List<UserPlaygroundListModel>>();
            model.Add(myPlaygrounds);
            model.Add(nearbyPlaygrounds);

            return model;
        }

        // Creates a PlaygroundRating and returns the updated average rating for the playground
        [HttpPost]
        public double RatePlayground(string playgroundId, string rating)
        {
            int playgroundIdInt = Int32.Parse(playgroundId);
            int ratingInt = Int32.Parse(rating);
            pService.RatePlayground(User.Identity.GetUserId(), playgroundIdInt, ratingInt);
            return pService.GetAverageRatingForPlayground(playgroundIdInt);
        }


        // Notifications

        // Checks how many friend requests the current user has
        public int FriendNotification()
        {
            return cService.GetFriendNotifications(User.Identity.GetUserId());
        }

        // Checks how many unread posts the current user has
        public int FeedNotification()
        {
            string userId = User.Identity.GetUserId();
            List<ApplicationUser> friendList = uService.GetAllFriendsForUser(userId);
            List<Playground> playgrounds = pService.GetPlaygroundsForUser(userId);
            return fService.CheckForNewPosts(friendList, userId, playgrounds);
        }

        // Updates the latest date and time the current user opened the feed to be able to count how many unread posts
        public string UpdateLatestRead()
        {
            uService.UpdateLatestRead(User.Identity.GetUserId());
            return "OK";
        }

    }
}
