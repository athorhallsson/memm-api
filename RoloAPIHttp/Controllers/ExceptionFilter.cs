﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using System.Net.Mail;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Web.Http.Filters;

namespace RoloAPIHttp.Filters
{
    public class ExceptionFilter : ExceptionFilterAttribute 
    {
        public override void OnException(HttpActionExecutedContext context)
        {
          /*  if (context.Exception is NotImplementedException)
            {
                context.Response = new HttpResponseMessage(HttpStatusCode.NotImplemented);
            } */
            // Logs exceptions to file, with email and console
            LogMessage(context.Exception.Message);
        }

        private void LogMessage(string message)
        {
            string fromEmail = ConfigurationManager.AppSettings["FromEmail"];
            string errorHandlingEmail = ConfigurationManager.AppSettings["ErrorHandlingEmail"];

            try
            {
                using (MailMessage emailMessage = new MailMessage())
                {
                    emailMessage.To.Add(errorHandlingEmail);
                    emailMessage.Subject = "An error occured.";
                    emailMessage.Body = message;
                    using (SmtpClient client = new SmtpClient())
                    {
                        client.EnableSsl = true;
                        client.Send(emailMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(DateTime.Now);
                Debug.WriteLine(ex.Message);
                Debug.WriteLine(ex.StackTrace);
            }
        }
    }
}
