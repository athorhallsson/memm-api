﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using RoloAPIHttp.Models;
using System.Web;
using System.Data.Entity;

namespace RoloAPIHttp.DAL
{
    public interface IAppDataContext
    {
        IDbSet<ApplicationUser> Users { get; set; }
        IDbSet<Comment> Comments { get; set; }
        IDbSet<FriendConnection> FriendConnections { get; set; }
        IDbSet<Image> Images { get; set; }
        IDbSet<Playground> Playgrounds { get; set; }
        IDbSet<PlaygroundConnection> PlaygroundConnections { get; set; }
        IDbSet<PlaygroundFeature> PlaygroundFeatures { get; set; }
        IDbSet<PlaygroundRating> PlaygroundRatings { get; set; }
        IDbSet<Post> Posts { get; set; }
        IDbSet<CheckInConnection> CheckInConnections { get; set; }
        int SaveChanges();
    }
        
        
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IAppDataContext
    {
        public IDbSet<Comment> Comments { get; set; }
        public IDbSet<FriendConnection> FriendConnections { get; set; }
        public IDbSet<Image> Images { get; set; }
        public IDbSet<Playground> Playgrounds { get; set; }
        public IDbSet<PlaygroundConnection> PlaygroundConnections { get; set; }
        public IDbSet<PlaygroundFeature> PlaygroundFeatures { get; set; }
        public IDbSet<PlaygroundRating> PlaygroundRatings { get; set; }
        public IDbSet<Post> Posts { get; set; }
        public IDbSet<CheckInConnection> CheckInConnections { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}