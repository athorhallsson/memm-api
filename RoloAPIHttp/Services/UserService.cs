﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RoloAPIHttp.Models;
using RoloAPIHttp.ViewModels;
using RoloAPIHttp.DAL;
using System.Diagnostics;

namespace RoloAPIHttp.Services
{
    public class UserService
    {
        private readonly IAppDataContext db;

        public UserService(IAppDataContext dbContext)
        {
            db = dbContext ?? new ApplicationDbContext();
        }

        public MePageViewModel GetInfoForMe(string id)
        {
            var result = (from user in db.Users
                          where user.Id == id
                          select user).SingleOrDefault();

            MePageViewModel me = new MePageViewModel();
            me.Id = result.Id;
            me.Name = result.Name;
            me.DateOfBirth = result.DateOfBirth;
            me.ProfilePic = result.ProfilePic;

            return me;
        }

        public List<ApplicationUser> GetAllFriendsForUser(string id)
        {
            // Finds the users which added the current user
            List<ApplicationUser> friendList = (from friends in db.Users
                                                join friendcon in db.FriendConnections on friends.Id equals friendcon.FriendId.Id
                                                where friendcon.FriendId.Id != id && friendcon.UserId.Id == id && friendcon.Accepted == true
                                                orderby friends.Name ascending
                                                select friends).ToList();
            // Finds the users added by the current user
            List<ApplicationUser> friendList2 = (from friends in db.Users
                                                 join friendcon in db.FriendConnections on friends.Id equals friendcon.UserId.Id
                                                 where friendcon.UserId.Id != id && friendcon.FriendId.Id == id && friendcon.Accepted == true
                                                 orderby friends.Name ascending
                                                 select friends).ToList();
            friendList.AddRange(friendList2);
            return friendList;
        }

        public List<UserListViewModel> GetFriendsViewmodelsForUser(string id)
        {
            List<UserListViewModel> result = new List<UserListViewModel>();
            List<ApplicationUser> friendList = GetAllFriendsForUser(id);
            foreach (ApplicationUser user in friendList)
            {
                result.Add(makeUserListViewModel(user));
            }
            return result;
        }

        public List<UserListViewModel> GetRequestsViewModelsForUser(string id)
        {
            List<UserListViewModel> result = new List<UserListViewModel>();
            List<ApplicationUser> requestList = GetAllRequestsForUser(id);
            foreach (ApplicationUser user in requestList)
            {
                result.Add(makeUserListViewModel(user));
            }
            return result;
        }

        public List<ApplicationUser> GetAllRequestsForUser(string id)
        {
            List<ApplicationUser> requestList = (from friends in db.Users
                                                 join friendcon in db.FriendConnections on friends.Id equals friendcon.UserId.Id
                                                 where friendcon.UserId.Id != id && friendcon.FriendId.Id == id && friendcon.Accepted == false
                                                 orderby friends.Name ascending
                                                 select friends).ToList();
            return requestList; 
        }

        public UserListViewModel makeUserListViewModel(ApplicationUser u)
        {
            UserListViewModel result = new UserListViewModel();
            result.Id = u.Id;
            result.Name = u.Name;
            result.ProfilePic = (from img in db.Images
                                 where img.Id == u.ProfilePic.Id
                                 select img).SingleOrDefault();
            result.Username = u.UserName;
            int activePlaygroundId = (from cc in db.CheckInConnections
                                      where cc.UserId.Id == u.Id
                                      select cc.PlaygroundId.Id).SingleOrDefault();
            if (activePlaygroundId != 0)
            {
                result.ActivePlaygroundName = (from p in db.Playgrounds
                                               where p.Id == activePlaygroundId
                                               select p.Name).Single();
            }
            else
            {
                result.ActivePlaygroundName = "";
            }
            return result;
        }

        public UserPageViewModel GetInfoForUser(string username)
        {
            UserPageViewModel result = new UserPageViewModel();
            string userId = (from u in db.Users
                             where u.UserName == username
                             select u.Id).Single();
            result.User = this.GetInfoForMe(userId);

            List<Playground> playgroundList = (from playground in db.Playgrounds
                                               join pc in db.PlaygroundConnections
                                               on playground.Id equals pc.PlaygroundId.Id
                                               where pc.UserId.Id == userId
                                               select playground).ToList();
            List<UserPlaygroundListModel> result2 = new List<UserPlaygroundListModel>();
            foreach (Playground p in playgroundList)
            {
                UserPlaygroundListModel temp = new UserPlaygroundListModel();
                temp.Id = p.Id;
                temp.Name = p.Name;
                temp.Latitude = p.Latitude;
                temp.Longitude = p.Longitude;
                temp.Range = 0;
                result2.Add(temp);
            }
            result.PlaygroundList = result2;
            return result;
        }

        public List<UserListViewModel> GetRequestUserViewModelsForSearch(string search, string userId)
        {
            List<UserListViewModel> result = new List<UserListViewModel>();
            List<ApplicationUser> requestList = GetRequestUsersFromSearch(search, userId);
            foreach (ApplicationUser user in requestList)
            {
                result.Add(makeUserListViewModel(user));
            }
            return result;
        }

        public List<ApplicationUser> GetRequestUsersFromSearch(string searchString, string id)
        {
            List<ApplicationUser> userSearchList = (from friends in db.Users
                                                    join friendcon in db.FriendConnections on friends.Id equals friendcon.UserId.Id
                                                    where (friendcon.UserId.Id != id && friendcon.FriendId.Id == id && friendcon.Accepted == false) && (friends.Name.StartsWith(searchString) || friends.Name.Contains(searchString) || friends.Name.EndsWith(searchString))
                                                    orderby friends.Name ascending
                                                    select friends).ToList();
            return userSearchList;
        }

        public List<UserListViewModel> GetFriendsUserViewModelsForSearch(string search, string userId)
        {
            List<UserListViewModel> result = new List<UserListViewModel>();
            List<ApplicationUser> requestList = GetFriendsUsersFromSearch(search, userId);
            foreach (ApplicationUser user in requestList)
            {
                result.Add(makeUserListViewModel(user));
            }
            return result;
        }

        public List<ApplicationUser> GetFriendsUsersFromSearch(string searchString, string id)
        {
           List<ApplicationUser> userSearchList = (from friends in db.Users
                                                    join friendcon in db.FriendConnections on friends.Id equals friendcon.UserId.Id
                                                    where (friendcon.UserId.Id != id && friendcon.FriendId.Id == id && friendcon.Accepted == true) && (friends.Name.StartsWith(searchString) || friends.Name.Contains(searchString) || friends.Name.EndsWith(searchString))
                                                    select friends).ToList();
            List<ApplicationUser> userSearchList2 = (from friends in db.Users
                                                     join friendcon in db.FriendConnections on friends.Id equals friendcon.FriendId.Id
                                                     where (friendcon.FriendId.Id != id && friendcon.UserId.Id == id && friendcon.Accepted == true) && (friends.Name.StartsWith(searchString) || friends.Name.Contains(searchString) || friends.Name.EndsWith(searchString))
                                                     select friends).ToList();
            userSearchList.AddRange(userSearchList2);
            userSearchList.OrderBy(p => p.Name).ToList();
            return userSearchList;
        }

        public List<UserListViewModel> GetOthersUserViewModelsForSearch(string search, string userId)
        {
            List<UserListViewModel> result = new List<UserListViewModel>();
            List<ApplicationUser> requestList = GetOthersUsersFromSearch(search, userId);
            foreach (ApplicationUser user in requestList)
            {
                result.Add(makeUserListViewModel(user));
            }
            return result;
        }

        public List<ApplicationUser> GetOthersUsersFromSearch(string searchString, string id)
        {
           List<ApplicationUser> allUsers = (from friends in db.Users
                                              where friends.Id != id && (friends.Name.StartsWith(searchString) || friends.Name.Contains(searchString) || friends.Name.EndsWith(searchString))
                                              orderby friends.Name ascending
                                              select friends).ToList();

            List<ApplicationUser> friendUsers = GetAllFriendsForUser(id);
            List<ApplicationUser> requestUsers = GetAllRequestsForUser(id);
            friendUsers.AddRange(requestUsers);

            List<ApplicationUser> userSearchList = allUsers.Except(friendUsers).ToList();
            return userSearchList;
        }

        public void AddImageToUser(Image img, string userId)
        {
            ApplicationUser user = (from u in db.Users
                                    where u.Id == userId
                                    select u).Single();
            user.ProfilePic = img;
            db.SaveChanges();
        }

        public void UpdateLatestRead(string userId)
        {
            var result = (from u in db.Users
                          where u.Id == userId
                          select u).Single();
            // Sets the latest read to UTC time to avoid error from using ther server time
            result.LatestRead = DateTime.UtcNow;
            db.SaveChanges();
        }
    }
}