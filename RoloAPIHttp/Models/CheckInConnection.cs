﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoloAPIHttp.Models
{
    public class CheckInConnection
    {
        public int Id { get; set; }
        public virtual ApplicationUser UserId { get; set; }
        public virtual Playground PlaygroundId { get; set; }
        public DateTime DateCreated { get; set; }
    }
}