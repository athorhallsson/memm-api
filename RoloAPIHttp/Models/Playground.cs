﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoloAPIHttp.Models
{
    public class Playground
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual PlaygroundFeature Features { get; set; }
        public virtual Image Image { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}