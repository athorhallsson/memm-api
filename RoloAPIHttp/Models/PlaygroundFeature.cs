﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RoloAPIHttp.Models
{
    public class PlaygroundFeature
    {
        public int Id { get; set; }
        public bool InfantSwings { get; set; }
        public bool Swings { get; set; }
        public bool Sandbox { get; set; }
        public bool InfantSlide { get; set; }
        public bool Slide { get; set; }
        public bool InfantJungleGym { get; set; }
        public bool JungleGym { get; set; }
        public bool SeeSaw { get; set; }
        public bool RockingHorse { get; set; }
        public bool BalancingEquipment { get; set; }
        public bool Goal { get; set; }
        public bool Basket { get; set; }
        public bool PlayHouse { get; set; }
    }
}